package programas;
import programas.clientes.*;

import java.util.Scanner;

    /*
    @description: It serves to register or delete the customers info from a database.
    @author: Adrian Antanyon, Alex Gonzalez, Javier Ruiz
    @version: 26/02/2020
     */


public class clients_negoci {
    final int maxclients = 100;
    final int maxInformacion = 6;

    Scanner lector = new Scanner(System.in);

    public static void main(String[] args) {

        clients_negoci programa = new clients_negoci();
        programa.inici();

    }

    public void inici(){
        int seleccion=0, i=0;
        String [][] clientes = new String[maxclients][maxInformacion];

        do {
            menu();
            comprobacion();
            seleccion = lector.nextInt();

            switch (seleccion){
                case 1:
                    new AltaCliente(clientes);
                    break;
                case 2:
                    new visualizarCliente(clientes);
                    break;
                case 3:
                    new BajaCliente(clientes);
                    break;
                case 4:
                    new RecuperarCliente(clientes);
                    break;
                case 5:
                    new OrdenarClientes(clientes);
                    break;
                case 6:
                    new BuscarCliente(clientes);
                    break;
                case 7:
                    System.out.println("Saliendo del programa.");
                    break;
                default:
                    System.out.println("Opción no válida");
            }

        }while (seleccion != 7);
    }

    public void menu(){
        System.out.println("Seleccione la opción que desea, por favor:\n" +
                "1) Dar de alta\n" +
                "2) Visualizar clientes\n" +
                "3) Dar de baja \n" +
                "4) Recuperar datos de cliente\n" +
                "5) Ordenar clientes\n" +
                "6) Buscar cliente\n" +
                "7) Salir");
    }

    public void comprobacion(){

        while (!lector.hasNextInt()){
            System.out.println("Datos incorrectos, vuelva a introducirlo, por favor");
            lector.next();
        }
    }
}
